#include <stdio.h>
#include <stdlib.h>

#include "util.h"

int main(int argc, char* argv[])
{
    int sum = 0;

    // Returns first token
    char* token = strtok(argv[1], STRING_SEPARATOR);

    // Keep printing tokens while one of the
    // delimiters present in the given string
    printf("Tokes are:\n----------");

    separateStr(token, sum);

    printf("\n----------");

    // Print sum of all numbers
    printf("\nSum = %d\n", sum);

    return EXIT_SUCCESS;
}
