#include <stdio.h>
#include <stdlib.h>

#include "util.h"

void separateStr(char* token, int sum){

    int num = 0;

    while(token != NULL){

        // Convert all tokens to integers
        num = atoi(token);

        // Print converted integer values
        printf("\nString value \"%s\" - integer value %d\n", token, num);

        sum += num;

        token = strtok(NULL, "-");
    };
}
